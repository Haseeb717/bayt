class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :title
      t.string :car_type
      t.string :price
      t.text :url

      t.timestamps null: false
    end
  end
end
