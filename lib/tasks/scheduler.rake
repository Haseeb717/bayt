desc "This task is called to send VideoProject emails"
task :send_projects_email => :environment do
  require 'open-uri'

  @url = ['http://uae.yallamotor.com/new-cars/honda','http://uae.yallamotor.com/new-cars/ford','http://uae.yallamotor.com/new-cars/bmw','http://uae.yallamotor.com/new-cars/chevrolet',
    'http://uae.yallamotor.com/new-cars/toyota','http://uae.yallamotor.com/new-cars/kia','http://uae.yallamotor.com/new-cars/hyundai','http://uae.yallamotor.com/new-cars/mercedes-benz',
    'http://uae.yallamotor.com/new-cars/mitsubishi','http://uae.yallamotor.com/new-cars/nissan']

  @url.each do |url|
    begin
    doc = Nokogiri::HTML(open(url))
      doc.css(".new-cars-results-box > .new-car-name").each do |item|
        type = item.text
        link = "http://uae.yallamotor.com"+item[:href]
        nested_doc = Nokogiri::HTML(open(link))
        spec_price = nested_doc.css(".prices-and-specs") 
        spec_price.css(".new-cars-results > .new-cars-results-text").each do |car_detail|
          title = car_detail.at('h4 a').text
          price = car_detail.at('h4 span').text
          Car.find_or_create(:car_type=>type,:title=>title,:price=>price,:url=>url)
        end
      end
    rescue
    end
  end
end